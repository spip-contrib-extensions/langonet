<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/langonet.git

return [

	// L
	'langonet_description' => 'Ce plugin permet d’exécuter des actions de vérification et de nettoyage des fichiers de langue de SPIP, des plugins ou des squelettes. Il offre aussi la possibilité de consulter l’ensemble des items contenus dans les fichiers de langue disponibles sur le site et d’effectuer des recherches d’items dans les fichiers de langue de SPIP. Sous certaines conditions, il produit automatiquement des items de langue à partir des textes libres repérés.
	Depuis la version 1.4.0, le plugin permet aussi d’éditer tout ou partie des items d’un fichier de langue.',
	'langonet_slogan' => 'Vérifier, rechercher, afficher, générer ou éditer les items de langue',
];
