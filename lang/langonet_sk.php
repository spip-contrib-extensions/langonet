<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/langonet-langonet?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_corriger' => 'Získať opravy',
	'bouton_generer' => 'Vytvoriť',
	'bouton_lister' => 'Zobraziť',
	'bouton_rechercher' => 'Vyhľadať',
	'bouton_verifier' => 'Potvrdiť',
	'bulle_afficher_fichier_lang' => 'Zobraziť jazykový súbor vytvorený @date@',
	'bulle_afficher_fichier_log' => 'Zobraziť protokol @date@',
	'bulle_corriger' => 'Stiahnuť opravený jazykový súbor',
	'bulle_telecharger_fichier_lang' => 'Stiahnuť jazykový súbor vytvorený @date@',
	'bulle_telecharger_fichier_log' => 'Stiahnuť protokol @date@',

	// I
	'info_arborescence_scannee' => 'Vyberte si priečinok, do ktorého sa stromová štruktúra naskenuje',
	'info_fichier_liste' => 'Vyberte si jazykový súbor, ktorého položky chcete zobraziť, s pomedzi tých súborov, ktoré sa nachádzajú na stránke.',
	'info_fichier_verifie' => 'Vyberte si jazykový súbor na schválenie z tých, ktoré sú na stránke.',
	'info_generer' => 'Táto možnosť vám umožňuje vytvoriť z jazyka originálu jazykový súbor daného modulu pre cieľový jazyk. Ak už cieľový jazyk existuje, jeho obsah sa pri vytváraní nového súboru použije znova.',
	'info_langue' => 'Skratka jazyka (príklad: <em>fr</em>, <em>en</em>, <em>es</em>, <em>sk</em> atď.)',
	'info_lister' => 'Táto možnosť vám umožní zobraziť položky jazykového súboru v abecednom poradí ',
	'info_mode' => 'Zhoduje sa s reťazcom, ktorý bude vložený pri vytváraní novej položky cieľového jazyka.',
	'info_pattern_item_cherche' => 'Zadajte reťazec, ktorý sa týka celej skratky položky jazyka alebo jej časti. Pri vyhľadávaní sa nikdy nezohľadňujú VEĽKÉ a malé písmená.',
	'info_pattern_texte_cherche' => 'Zadajte reťazec, ktorý sa týka celého prekladu francúzskeho reťazca alebo jeho časti. Pri vyhľadávaní sa nikdy nezohľadňujú VEĽKÉ a malé písmená.',
	'info_rechercher_item' => 'Táto možnosť vám umožňuje vyhľadať jazykové položky vo všetkých  jazykových súboroch na stránke. Kvôli výkonu boli zoskenované len francúzske jazykové súbory.',
	'info_rechercher_texte' => 'Táto možnosť vám umožňuje vyhľadať jazykové položky pomocou ich francúzskeho prekladu v jazykových súboroch  SPIPu <em>ecrire_fr,</em> <em>public_fr</em> a <em>spip_fr.</em> Cieľom vyhľadávanie je pred vytvorením textu skontrolovať, či rovnaký text už v SPIPe neexistuje.',
	'info_table' => 'Nižšie môžete vidieť abecedný zoznam jazykových položiek súboru <em>"@langue@"</em> (@total@). V každom bloku sú zobrazené položky, ktoré sa začínajú na rovnaké písmeno, skratka tučným písmom a za ňou text. Ak nad začiatočným písmenom prejdete myšou, zobrazí sa príslušný zoznam.',
	'info_verifier' => 'Táto možnosť vám na jednej strane umožňuje porovnať dve alternatívy v jazykových súboroch modulu. Môžete buď skontrolovať, či jazykové položky, ktoré sa používajú v niekoľkých jazykových súboroch (napríklad v zásuvnom module), sú definované v správnom jazykovom súbore, alebo či sa všetky definované položky stále používajú.<br />Okrem toho si môžete nechať vypísať a opraviť všetky prípady použitia funkcie _L() v súboroch PHP danej stromovej štruktúry.',

	// L
	'label_arborescence_scannee' => 'Stromová štruktúra, ktorú treba naskenovať',
	'label_correspondance' => 'Typ zhody',
	'label_correspondance_commence' => 'Začať od',
	'label_correspondance_contient' => 'Obsahuje',
	'label_correspondance_egal' => 'Je rovný',
	'label_fichier_liste' => 'Jazykový súbor',
	'label_fichier_verifie' => 'Jazyk na schválenie',
	'label_langue_cible' => 'Cieľový jazyk',
	'label_mode' => 'Spôsob vytvárania nových položiek',
	'label_pattern' => 'Reťazec, ktorý treba vyhľadať',
	'label_verification' => 'Typ scvaľovania',
	'label_verification_definition' => 'Zistenie chýbajúcich definícií',
	'label_verification_utilisation' => 'Zistenie zastaraných definícií',
	'legende_resultats' => 'Výsledky schvaľovania',
	'legende_table' => 'Zoznam položiek vybraného jazykového súboru',
	'legende_trouves' => 'Zoznam nájdených položiek (@total@)',

	// M
	'message_nok_champ_obligatoire' => 'Toto pole je povinné',
	'message_nok_ecriture_fichier' => 'Jazykový súbor "<em>@langue@</em>" modulu "<em>@module@</em>" sa nepodarilo vytvoriť, pretože pri jeho zápise nastala chyba!',
	'message_nok_item_trouve' => 'Podmienkam vyhľadávania nevyhovuje žiadna jazyková položka!',
	'message_ok_fichier_genere' => 'Jazykový súbor "<em>@langue@</em>" modulu "<em>@module@</em>" bol vytvorený správne.<br />Súbor môžete získať "<em>@fichier@</em>".',
	'message_ok_fonction_l_0' => 'V priečinku <em>@ou_fichier@</em> nebol zistený žiaden prípad použitia funkcie _L() v súboroch PHP.',
	'message_ok_fonction_l_1' => 'Keď sa v súboroch PHP priečinka <em>"@ou_fichier@"</em> zistí jedno použitie funkcie _L():',
	'message_ok_fonction_l_n' => 'V súboroch PHP v priečinku <em>@ou_fichier@</em> bol nájdený @nberr@ prípad použitia funkcie _L():',
	'message_ok_item_trouve' => 'Vyhľadávanie reťazca @pattern@ bolo úspešné.',
	'message_ok_item_trouve_commence_1' => 'Jazyková položka sa začína hľadaným reťazcom:',
	'message_ok_item_trouve_commence_n' => 'Na hľadaný reťazec sa začína @sous_total@ položiek:',
	'message_ok_item_trouve_contient_1' => 'Jazyková položka obsahuje hľadaný reťazec:',
	'message_ok_item_trouve_contient_n' => 'Les @sous_total@ položiek obsahuje celý hľadaný reťazec:',
	'message_ok_item_trouve_egal_1' => 'Jazyková položka presne zodpovedá vyhľadávanému reťazcu:',
	'message_ok_item_trouve_egal_n' => 'Hľadanému reťazcu zodpovedá @sous_total@ položiek:',
	'message_ok_table_creee' => 'Tabuľka položiek jazykového súboru @langue@ bola úspešne vytvorená.',

	// O
	'onglet_generer' => 'Vytvoriť jazyk',
	'onglet_lister' => 'Zobraziť jazyk',
	'option_aucun_dossier' => 'nevybrali ste žiadnu stromovú štruktúru',
	'option_aucun_fichier' => 'nevybrali ste žiaden jazyk',
	'option_mode_index' => 'Položka zdrojového jazyka',
	'option_mode_valeur' => 'Reťazec v zdrojovom jazyku',
	'option_mode_vide' => 'Prázdny reťazec',

	// T
	'titre_bloc_langues_generees' => 'Jazykové súbory',
	'titre_form_generer' => 'Vytvorenie jazykového súboru',
	'titre_form_lister' => 'Zobrazenie jazykových súborov',
	'titre_form_rechercher_item' => 'Vyhľadávanie skratiek v jazykových súboroch',
	'titre_form_rechercher_texte' => 'Vyhľadávanie v textoch jazykových súborov SPIPu',
	'titre_form_verifier' => 'Kontrola jazykových súborov',
];
