<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/langonet-langonet?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_cocher_aucun' => 'Check all', # MODIF
	'bouton_cocher_tout' => 'Uncheck all', # MODIF
	'bouton_corriger' => 'Retrieve the corrections',
	'bouton_generer' => 'Generate',
	'bouton_lister' => 'Display',
	'bouton_rechercher' => 'Search',
	'bouton_verifier' => 'Verify',
	'bulle_afficher_fichier_lang' => 'Display the language file generated on @date@',
	'bulle_afficher_fichier_log' => 'Display the log of @date@',
	'bulle_corriger' => 'Download the corrected language file',
	'bulle_telecharger_fichier_lang' => 'Download the language file generated on @date@',
	'bulle_telecharger_fichier_log' => 'Download the log file of @date@',

	// I
	'info_arborescence_scannee' => 'Choose the root folder for which all subdirectories will be scanned',
	'info_fichier_liste' => 'Choose the language file from those available on the site for which you want to display the items.',
	'info_fichier_verifie' => 'Choose the language file you wish to verify from those available on the site.',
	'info_generer' => 'This option allows you to generate, from a source language, the language file for a module in a target language. If the target file already exists, its content will be reused when creating the new file.',
	'info_langue' => 'Language code (for example: <em>fr</em>, <em>en</em>, <em>es</em>...)',
	'info_lister' => 'This option allows you to display the items in a language file listed in alphabetical order.',
	'info_mode' => 'Corresponds to the string which will be inserted at creation of a new target language item.',
	'info_pattern_item_cherche' => 'Enter a string corresponding entirely or partially to a language item shortcut. The search is allways case insensitive.',
	'info_pattern_texte_cherche' => 'Enter a string corresponding entirely or partially to a French translation of a language item. The search is allways case insensitive.',
	'info_rechercher_item' => 'This option allows you to search for language items in all the language files available on the site. For performance reasons, only French language files will be scanned.',
	'info_rechercher_texte' => 'This option allows you to search for language items by their French translation in the SPIP language files <em>ecrire_fr</em>, <em>public_fr</em> and <em>spip_fr</em>. The goal is to check whether a text already exists in SPIP before creating it yourself.',
	'info_table' => 'You can consult below the alphabetical list of language items of the file "<em>@langue@</em>" (@total@). Each block displays items with the same initial, the bold shortcut and the text displayed next. Hover over an initial to display the corresponding list.',
	'info_verifier' => 'This option allows you, on one hand, to check the language files for a given module in two complementary angles. It’s possible, whether checking if language items used in a group of files (a plugin, for example) are not defined in the suitable language file, whether some defined language items are no longer used. <br />On the other hand, it is possible to list and correct all uses of the function _L() in PHP files in a given tree.',

	// L
	'label_arborescence_scannee' => 'Directory tree to be scanned',
	'label_correspondance' => 'Match type',
	'label_correspondance_commence' => 'Begins by',
	'label_correspondance_contient' => 'Contents',
	'label_correspondance_egal' => 'Equal to',
	'label_fichier_liste' => 'Language file',
	'label_fichier_verifie' => 'Language to verify',
	'label_langue_cible' => 'Target language',
	'label_mode' => 'Creation mode of new items',
	'label_pattern' => 'String to search',
	'label_verification' => 'Type of verification',
	'label_verification_definition' => 'Detection of missing definitions',
	'label_verification_utilisation' => 'Detection of obsolete definitions',
	'legende_resultats' => 'Verification results',
	'legende_table' => 'List of items of the selected language file',
	'legende_trouves' => 'List of found items (@total@)',

	// M
	'message_nok_champ_obligatoire' => 'This field is required',
	'message_nok_ecriture_fichier' => 'The language file "<em>@langue@</em>" of the module "<em>@module@</em>" has not been created because an error occured during its writing !',
	'message_nok_item_trouve' => 'No language item matches the search!',
	'message_ok_fichier_genere' => 'The language file  "<em>@langue@</em>" of the module "<em>@module@</em>" has been correctly created.<br />You can retrieve the file "<em>@fichier@</em>".',
	'message_ok_fonction_l_0' => 'No use of the function _L() has been detected in the PHP files of the folder "<em>@ou_fichier@</em>".',
	'message_ok_fonction_l_1' => 'Only one use of the function _L() has been detected in the PHP files of the folder "<em>@ou_fichier@</em>":',
	'message_ok_fonction_l_n' => '@nberr@ cases of use of the function _L() were detected in PHP files from the directory "<em>@ou_fichier@</em>":',
	'message_ok_item_trouve' => 'The search for the string @pattern@ is successful.',
	'message_ok_item_trouve_commence_1' => 'The language item below begins by the search string:',
	'message_ok_item_trouve_commence_n' => 'The @sous_total@ language items below all begin by the search string:',
	'message_ok_item_trouve_contient_1' => 'The language item below contains the searched string:',
	'message_ok_item_trouve_contient_n' => 'The @sous_total@ items below contain all the searched string:',
	'message_ok_item_trouve_egal_1' => 'The item below correspond exactly to the search string:',
	'message_ok_item_trouve_egal_n' => 'The @sous_total@ items below correspond exactly to the search string:',
	'message_ok_table_creee' => 'The table of items od the language file @langue@ has been correctly created',

	// O
	'onglet_generer' => 'Generate a language',
	'onglet_lister' => 'Display a language',
	'option_aucun_dossier' => 'no directory tree selected',
	'option_aucun_fichier' => 'no language selected',
	'option_mode_index' => 'Item of the source language',
	'option_mode_valeur' => 'String in the source language',
	'option_mode_vide' => 'An empty string',

	// T
	'titre_bloc_langues_generees' => 'Language files',
	'titre_form_generer' => 'Creation of language files',
	'titre_form_lister' => 'Display of language files',
	'titre_form_rechercher_item' => 'Search of shortcuts in the language files',
	'titre_form_rechercher_texte' => 'Search of texts in the SPIP language files',
	'titre_form_verifier' => 'Verification of the language files',
];
