# Changelog

## Unreleased

### Fixed

- Retrouver les icones de téléchargement et d'affichage des fichiers résultat

## 2.3.0 - 2024-10-26

### Added

- !2 Lecture des fichiers de langue avec `return` plutôt que peuplant une globale
- !2 Écriture des fichiers de langue avec `return`, désactivé par défaut, mettre `define('_LANGONET_SYNTAXE', 'global');` dans `mes_options.php` pour activer
