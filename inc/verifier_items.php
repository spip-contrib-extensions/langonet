<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Mode debug inactif par défaut
if (!defined('_LANGONET_DEBUG')) {
	define('_LANGONET_DEBUG', false);
}

// Items de langue dans les fichiers PHP
// Fontions PHP _T ou _U avec apostrophe
if (!defined('_LANGONET_ITEM_PHP_TRAD_A')) {
	define('_LANGONET_ITEM_PHP_TRAD_A', '%_[TU]\s*[(]\s*\'(?:([a-z0-9_]+):)?([^\']*)\'\s*([^.,)]*[^)]*)%Sm');
}
// Fontions PHP _T ou _U avec guillemet
if (!defined('_LANGONET_ITEM_PHP_TRAD_G')) {
	define('_LANGONET_ITEM_PHP_TRAD_G', '%_[TU]\s*[(]\s*"(?:([a-z0-9_]+):)?([^"]*)"\s*([^.,)]*[^)]*)%Sm');
}
// déclaration d'items dans base/module.php avec apostrophe
if (!defined('_LANGONET_ITEM_PHP_OBJET_A')) {
	define('_LANGONET_ITEM_PHP_OBJET_A', '%=>\s*\'(?:([a-z0-9_]+):)([^\']*)\'\s*([^,\n]*)[,\n]%Sm');
}
// déclaration d'items dans base/module.php avec guillemet
if (!defined('_LANGONET_ITEM_PHP_OBJET_G')) {
	define('_LANGONET_ITEM_PHP_OBJET_G', '%=>\s*"(?:([a-z0-9_]+):)([^\/"]*)"%Sm');
}
// Utilisation de la forme complète dans les déclarations de Saisies
if (!defined('_LANGONET_ITEM_PHP_SAISIE')) {
	define('_LANGONET_ITEM_PHP_SAISIE', '%=>\s*[\'"]<:(?:([a-z0-9_-]+):)?((?:[\w]+(?:<[^>]*>)?)*)([^:]*):>[\'"]%sm');
}

// Items de langue dans les fichiers HTML

// Balise <:module:raccourci:> et toutes les formes admises avec paramètres et filtres
if (!defined('_LANGONET_ITEM_HTML_BALISE')) {
	define('_LANGONET_ITEM_HTML_BALISE', '%<:(?:([a-z0-9_-]+):)?((?:[^:<>|{]+(?:<[^>]*>)?)*)([^:>]*):/?>%sm');
}
// Fonction |singulier_ou_pluriel{arg1, arg2, nb} pour chaque argument.
// L'argument nb ou une fin de ligne est indispensable pour la détection de la fin de l'arg2
if (!defined('_LANGONET_ITEM_HTML_PLURIEL_ARG1')) {
	define('_LANGONET_ITEM_HTML_PLURIEL_ARG1', "%\|singulier_ou_pluriel{(?:[\s]*(?:(?:#[A-Z_0-9]+{)*)(?:([a-z0-9_-]+):)?([a-z0-9_]*))([^,]*),%sm");
}
if (!defined('_LANGONET_ITEM_HTML_PLURIEL_ARG2')) {
	define('_LANGONET_ITEM_HTML_PLURIEL_ARG2', "%\|singulier_ou_pluriel{[^,]*,(?:[\s]*(?:(?:#[A-Z_0-9]+{)*)(?:([a-z0-9_-]+):)([a-z0-9_]+))([^,\n]*)[,\n]%sm");
}
// Fonction _T : on détecte module:item uniquement pour éviter les faux positifs
if (!defined('_LANGONET_ITEM_HTML_FILTRE_T')) {
	define('_LANGONET_ITEM_HTML_FILTRE_T', "%#[A-Z_0-9]+{(?:([a-z0-9_-]+):)([a-z0-9_]+)}((?:\s*\|\w+(?:{[^.<>]*})?)*)\s*\|_T%Usm");
}
// Variable par #SET utilisable dans le filtre _T : on détecte module:item uniquement
if (!defined('_LANGONET_ITEM_HTML_SET')) {
	define('_LANGONET_ITEM_HTML_SET', "%#SET{[^,]+,\s*(?:([a-z0-9_-]+):)([a-z0-9_]+)}%Usm");
}

// Items de langue dans les fichiers YAML
if (!defined('_LANGONET_ITEM_YAML')) {
	define('_LANGONET_ITEM_YAML', ',<:(?:([a-z0-9_-]+):)?([a-z0-9_]+):>,sm');
}

// Items de langue dans les fichiers XML
// -- pour plugin.xml
if (!defined('_LANGONET_ITEM_PLUGINXML')) {
	define('_LANGONET_ITEM_PLUGINXML', ",<titre>\s*(?:([a-z0-9_-]+):)?([a-z0-9_]+)\s*</titre>,ism");
}
// -- pour paquet.xml
if (!defined('_LANGONET_ITEM_PAQUETXML')) {
	define('_LANGONET_ITEM_PAQUETXML', ",titre=['\"](?:([a-z0-9_-]+):)?([a-z0-9_]+)['\"],ism");
}
// -- pour les autres fichiers XML
if (!defined('_LANGONET_ITEM_XML_CONTENU')) {
	define('_LANGONET_ITEM_XML_CONTENU', ",<\w+>\s*(?:<:)*(?:([a-z0-9_-]+):)([a-z0-9_]+)(?::>)*\s*</\w+>,ism");
}
if (!defined('_LANGONET_ITEM_XML_ATTRIBUT')) {
	define('_LANGONET_ITEM_XML_ATTRIBUT', ",\w+=['\"](?:([a-z0-9_-]+):)([a-z0-9_]+)['\"],ism");
}

// On définit la liste des regexp de chaque type de fichier avec la chaine de la constante et pas la constante elle-même.
// Il faut donc utiliser constant() pour avoir la valeur de la regexp.
$GLOBALS['langonet_regexp'] = array(
	'paquet.xml' => array('_LANGONET_ITEM_PAQUETXML'),
	'plugin.xml' => array('_LANGONET_ITEM_PLUGINXML'),
	'xml'        => array(
		'_LANGONET_ITEM_XML_CONTENU',
		'_LANGONET_ITEM_XML_ATTRIBUT'
	),
	'yaml' => array('_LANGONET_ITEM_YAML'),
	'html' => array(
		'_LANGONET_ITEM_HTML_BALISE',
		'_LANGONET_ITEM_HTML_PLURIEL_ARG1',
		'_LANGONET_ITEM_HTML_PLURIEL_ARG2',
		'_LANGONET_ITEM_HTML_FILTRE_T',
		'_LANGONET_ITEM_HTML_SET'
	),
	'php' => array(
		'_LANGONET_ITEM_PHP_OBJET_A',
		'_LANGONET_ITEM_PHP_OBJET_G',
		'_LANGONET_ITEM_PHP_TRAD_A',
		'_LANGONET_ITEM_PHP_TRAD_G',
		'_LANGONET_ITEM_PHP_SAISIE'
	)
);

/**
 * Verification des items de langue non définis ou obsolètes.
 *
 * @param string $module       prefixe du fichier de langue
 * @param string $langue       index du nom de langue
 * @param string $ou_langue    chemin vers le fichier de langue à vérifier
 * @param array  $ou_fichiers  tableau des racines d'arborescence à vérifier
 * @param string $verification type de verification à effectuer
 *
 * @return array
 */
function inc_verifier_items($module, $langue, $ou_langue, $ou_fichiers, $verification) {

	// On constitue la liste des fichiers pouvant être susceptibles de contenir des items de langue.
	// Pour cela on boucle sur chacune des arborescences choisies.
	// - les ultimes sous-repertoires charsets/ , lang/ , req/ sont ignorés.
	// - seuls les fichiers php, html, xml ou yaml sont considérés.
	$fichiers = array();
	foreach ($ou_fichiers as $_arborescence) {
		$fichiers = array_merge(
			$fichiers,
			preg_files(_DIR_RACINE . $_arborescence, '(?<!/charsets|/lang|/req)(/[^/]*\.(xml|yaml|html|php))$')
		);
	}

	// On collecte l'ensemble des occurrences d'utilisation d'items de langue dans la liste des fichiers
	// précédemment constituée.
	$utilises = collecter_occurrences($fichiers);

	// On sauvegarde l'index de langue global si il existe car on va le modifier pendant le traitement.
	include_spip('inc/outiller');
	sauvegarder_index_langue_global();

	// On charge le fichier de langue a lister si il existe dans l'arborescence $chemin
	// (evite le mecanisme standard de surcharge SPIP)
	list($traductions, $fichier_langue) = charger_module_langue($module, $langue, $ou_langue);

	// On restaure l'index de langue global si besoin
	restaurer_index_langue_global();

	// Traitement des occurrences d'erreurs et d'avertissements et constitution de la structure de résultats
	if ($verification == 'definition') {
		// Les chaines definies sont dans les fichiers definis par la RegExp ci-dessous
		// Autrement dit les fichiers francais du repertoire lang/ sont la reference
		$fichiers_langue = preg_files(_DIR_RACINE, '/lang/[^/]+_fr\.php$');
		$resultats = reperer_items_non_definis($utilises, $module, $traductions, $fichiers_langue);
	} elseif ($traductions) {
		$resultats = reperer_items_non_utilises($utilises, $module, $traductions);
	}

	// Compléments de la structure de résultats
	$resultats['module'] = $module;
	$resultats['langue'] = $fichier_langue;
	$resultats['ou_fichier'] = $ou_fichiers;

	return $resultats;
}

/**
 * Cherche l'ensemble des occurrences d'utilisation d'items de langue dans la liste des fichiers fournie.
 * Cette recherche se fait ligne par ligne, ce qui ne permet pas de trouver les items sur plusieurs lignes.
 *
 * @param $fichiers
 *
 * @return array
 */
function collecter_occurrences($fichiers) {

	$utilises = array(
		'raccourcis'  => array(),
		'modules'     => array(),
		'items'       => array(),
		'occurrences' => array(),
		'debuts'      => array(),
		'fins'        => array(),
		'variables'    => array(),
		'indetermines'   => array(),
		'debug'       => array()
	);

	foreach ($fichiers as $_fichier) {
		if ($contenu = file_get_contents($_fichier)) {
			$type_fichier = identifier_type_fichier($_fichier);
			if (isset($GLOBALS['langonet_regexp'][$type_fichier])) {
				$regexps = $GLOBALS['langonet_regexp'][$type_fichier];
				// On stocke aussi le fichier à scanner sous forme d'un tableau de lignes afin de rechercher
				// les numéros de ligne et de colonne des occurrences
				$lignes = file($_fichier);
				foreach ($regexps as $_regexp) {
					if (preg_match_all(constant($_regexp), $contenu, $matches, PREG_OFFSET_CAPTURE)) {
						foreach ($matches[0] as $_cle => $_expression) {
							// Recherche de la ligne et de la colonne à partir de l'offset global de début
							// de l'expression
							list($ligne, $no_ligne, $no_colonne) = rechercher_ligne($_expression[1], $lignes);

							// Détermination d'une suite au raccourci
							$suite = isset($matches[3]) ? $matches[3][$_cle][0] : '';

							// Initialisation de l'occurrence avec sa complétion et mémorisation
							$occurrence = array(
								'fichier'     => $_fichier,
								'ligne'       => $ligne,
								'regexp'      => str_replace('_LANGONET_ITEM_', '', $_regexp),
								'match'       => $_expression[0],
								'suite'       => preg_replace('%\s%', '', $suite),
								'no_ligne'    => $no_ligne,
								'no_colonne'  => $no_colonne,
								'module'      => $matches[1][$_cle][0],
								'raccourci'   => preg_replace('%\s%', '', $matches[2][$_cle][0]),
								'debut'       => $matches[2][$_cle][0],
								'milieu'      => '',
								'fin'         => '',
								'variable'    => false,
								'indetermine' => false
							);

							// Mémorisation de l'occurrence dans la liste des utilisations
							$utilises = memoriser_occurrence($utilises, $occurrence, $_regexp);
						}
					}
				}
			} else {
				spip_log("Ce type de fichier n'est pas scanné : {$type_fichier} ({$_fichier})", 'langonet');
			}
		}
	}

	return $utilises;
}

/**
 * Identifie le type de fichier dans lequel chercher les occurrences d'utilisation d'items
 * de langue.
 *
 * @param string $fichier
 *                        Chemin complet du fichier à scanner
 *
 * @return string
 *                Extension du fichier parmi 'xml', 'yaml', 'html' et 'php' ou le nom du fichier de description
 *                du plugin 'paquet.xml' ou 'plugin.xml'.
 */
function identifier_type_fichier($fichier) {

	// On initialise le type avec l'extension du fichier
	$informations = pathinfo($fichier);
	$type = strtolower($informations['extension']);

	// Pour les fichiers XML on précise si le fichier est un paquet.xml ou un plugin.xml
	if ($type == 'xml') {
		if (($informations['basename'] == 'paquet.xml')
		or ($informations['basename'] == 'plugin.xml')) {
			$type = strtolower($informations['basename']);
		}
	}

	return $type;
}

function rechercher_ligne($offset, $lignes) {

	$no_ligne = $no_colonne = 0;
	$ligne = $lignes[0];

	$somme_ligne = 0;
	foreach ($lignes as $_no_ligne => $_ligne) {
		$longueur_ligne = strlen($_ligne);
		$somme_ligne += $longueur_ligne;
		if ($somme_ligne > $offset) {
			// on a trouvé la ligne
			$ligne = $_ligne;
			$no_ligne = $_no_ligne;
			// il faut déterminer la colonne
			$no_colonne = $longueur_ligne - ($somme_ligne - $offset);
			break;
		}
	}

	return array($ligne, $no_ligne, $no_colonne);
}

/**
 * Memorise selon une structure prédéfinie chaque occurrence d'utilisation d'un item.
 * Cette fonction analyse au passage si l'item est dynamique ou pas (_T avec $ ou concatenation).
 *
 * @param array $utilisations Tableau des occurrences d'utilisation des items de langues construit à chaque appel
 *                            de la fonction.
 * @param array  $occurrence  Tableau associatif définissant l'occurrence d'utilisation en cours de mémorisation.
 * @param string $regexp      Expression régulière utilisée pour trouver l'occurrence d'utilisation en cours de
 *                            mémorisation.
 *
 * @return array Le tableau des occurrences mis à jour avec l'occurrence passée en argument
 */
function memoriser_occurrence($utilisations, $occurrence, $regexp) {

	include_spip('inc/outiller');

	// Détection de la variabilité d'un raccourci (en PHP ou HTML uniquement) qu'il convient de nettoyer puis de qualifier.
	// -- Cas du PHP:
	if (in_array($regexp, $GLOBALS['langonet_regexp']['php'])) {
		// 1- Déterminer la variabilité du raccourci à partir de l'existence d'une suite.
		//    On met à jour les index debut, milieu et fin ainsi que le raccourci si il est variable
		if ($occurrence['suite']) {
			// Si il y a une suite il y a forcément un milieu
			$occurrence['milieu'] = $occurrence['suite'];

			// on cherche la fin du premier argument de _T ou _U et récupère la fin exact sans le deuxième paramètre
			// éventuel de la fonction
			$offset_virgule = strpos(trim($occurrence['milieu']), ',');
			if ($offset_virgule !== false) {
				$occurrence['milieu'] = substr($occurrence['milieu'], 0, $offset_virgule);
			}

			// Maintenant on peut vérifier si la suite contient une fin non variable qui permettrait de mieux
			// identifier le raccourci
			if ($occurrence['milieu']) {
				$raccourci_partiellement_variable = true;
				$occurrence['variable'] = true;
				if (preg_match('%[\'"]([a-z0-9_]+)[\'"]$%i', $occurrence['milieu'], $matches)) {
					$occurrence['fin'] = $matches[1];
					$occurrence['milieu'] = substr(
						$occurrence['milieu'],
						0,
						strlen($occurrence['milieu']) - strlen($occurrence['fin']) - 2
					);
					$occurrence['raccourci'] = $occurrence['debut']
						. ($occurrence['milieu'] ? '*' : '')
						. $occurrence['fin'];
				} elseif (preg_match('%[}\'"]([a-z0-9_]+)$%i', $occurrence['milieu'], $matches)) {
					$occurrence['fin'] = $matches[1];
					$occurrence['milieu'] = substr(
						$occurrence['milieu'],
						0,
						strlen($occurrence['milieu']) - strlen($occurrence['fin'])
					);
					$occurrence['raccourci'] = $occurrence['debut']
						. ($occurrence['milieu'] ? '*' : '')
						. $occurrence['fin'];
				} else {
					$occurrence['raccourci'] = $occurrence['debut']
						. ($occurrence['milieu'] ? '*' : '');
				}
			}
		}

		// 2- Déterminer la variabilité du raccourci à partir du raccourci lui-même car il se peut que le traitement
		//    de la suite ne soit pas suffisant.
		if (preg_match('#^([a-z0-9_*]*)(.*)$#im', $occurrence['raccourci'], $matches)) {
			if (!$matches[1]) {
				// Le raccourci est complètement variable, il est donc indéterminé.
				$raccourci_totalement_variable = true;
				$occurrence['variable'] = true;
				$occurrence['indetermine'] = true;
				$occurrence['raccourci'] = '*';
				$occurrence['debut'] = '';
			} elseif ($matches[2]) {
				// Le raccourci possède une partie variable que l'on extrait et que l'on décortique pour savoir
				// si la partie est un milieu ou une fin.
				$raccourci_partiellement_variable = true;
				$occurrence['variable'] = true;
				$occurrence['raccourci'] = $matches[1] . '*';
				$occurrence['debut'] = $matches[1];
				$occurrence['milieu'] = $matches[2];

				// On vérifie maintenant que le milieu est entièrement variable ou si il existe une fin récupérable
				if (preg_match('%}([a-z0-9_]+)$%i', $occurrence['milieu'], $matches)) {
					$occurrence['fin'] = $matches[1];
					$occurrence['milieu'] = substr(
						$occurrence['milieu'],
						0,
						strlen($occurrence['milieu']) - strlen($occurrence['fin'])
					);
					$occurrence['raccourci'] .= $occurrence['fin'];
				}
			}
		}
	}

	// -- Cas du HTML:
	if (in_array($regexp, $GLOBALS['langonet_regexp']['html'])) {
		if ($regexp !== '_LANGONET_ITEM_HTML_BALISE') {
			// Nettoyage de la suite pour certaines regexp
			if (
				($regexp === '_LANGONET_ITEM_HTML_PLURIEL_ARG2')
				and ($occurrence['suite'] === '}')
			) {
				// Ce n'est pas une suite, on là met à vide
				$occurrence['suite'] = '';
			}

			// 1- Déterminer la variabilité du raccourci à partir de l'existence d'une suite.
			//    Ce n'est possible que pour les cas d'utilisation de _T
			if ($occurrence['suite']) {
				$occurrence['variable'] = true;
				$occurrence['milieu'] = $occurrence['suite'];

				if (preg_match('%\|[\w_]+{([\w_]+)}$%i', $occurrence['milieu'], $matches)) {
						$occurrence['fin'] = $matches[1];
						$occurrence['milieu'] = substr(
							$occurrence['milieu'],
							0,
							strlen($occurrence['milieu']) - strlen($matches[0])
						);
						$occurrence['raccourci'] = $occurrence['debut']
							. ($occurrence['milieu'] ? '*' : '')
							. $occurrence['fin'];
				} elseif ($occurrence['milieu'] === '}') {
					// On est dans le cas entièrement variable du filtre singulier_ou_pluriel
					$occurrence['indetermine'] = true;
					$occurrence['module'] = '*';
					$occurrence['raccourci'] = '*';
					$occurrence['debut'] = '';
					$occurrence['milieu'] = '';
				} else {
					$occurrence['raccourci'] .= '*';
				}
			}
		}

		// 2- Déterminer la variabilité du raccourci à partir du raccourci lui-même : forcément entièrement variable
		if ($occurrence['suite'] and !$occurrence['raccourci']) {
			// Cas de la nouvelle écriture variable du raccourci <:xxx:{=#ENV{yyy}}:>
			$occurrence['milieu'] = $occurrence['suite'];
			$occurrence['variable'] = true;
			$occurrence['indetermine'] = true;
			$occurrence['raccourci'] = '*';
		}
	}

	// TODO : vérifier si avec les traitements précédents extraire_argument est encore nécessaire
//	list($raccourci) = extraire_arguments($occurrence['raccourci']);

	// Définition d'un index unique pour ranger l'occurrence indépendemment de son raccourci que peut ne pas l'être
	// TODO : si un raccourci est identique dans deux modules différents on va écraser l'index existant
	list($raccourci_unique) = calculer_raccourci_unique($occurrence['raccourci'], $utilisations['raccourcis']);
	$module = $occurrence['module'];
	$index = ($module ? "{$module}:{$raccourci_unique}" : $raccourci_unique);

	$utilisations['raccourcis'][$index] = $occurrence['raccourci'];
	$utilisations['modules'][$index] = $module;
	$utilisations['items'][$index] = ($module ? "{$module}:{$occurrence['raccourci']}" : $occurrence['raccourci']);
	$utilisations['occurrences'][$index][$occurrence['fichier']][$occurrence['no_ligne']][] = $occurrence;
	$utilisations['debuts'][$index] = $occurrence['debut'];
	$utilisations['fins'][$index] = $occurrence['fin'];
	$utilisations['variables'][$index] = $occurrence['variable'];
	$utilisations['indetermines'][$index] = $occurrence['indetermine'];

	// Construction d'une liste plate pour debug
	if (_LANGONET_DEBUG) {
		$utilisations['debug'][] = $occurrence;
	}

	return $utilisations;
}

/**
 * Gérer les arguments.
 *
 * La RegExp utilisee ci-dessous est defini dans phraser_html ainsi:
 * define('NOM_DE_BOUCLE', "[0-9]+|[-_][-_.a-zA-Z0-9]*");
 * define('NOM_DE_CHAMP', "#((" . NOM_DE_BOUCLE . "):)?(([A-F]*[G-Z_][A-Z_0-9]*)|[A-Z_]+)(\*{0,2})");
 *
 * @param string $raccourci_regexp
 *
 * @return array
 */
function extraire_arguments($raccourci_regexp) {

	include_spip('public/phraser_html');
	$arguments = array();
	if (preg_match_all('/' . NOM_DE_CHAMP . '/S', $raccourci_regexp, $matches, PREG_SET_ORDER)) {
		foreach ($matches as $_match) {
			$nom = strtolower($_match[4]);
			$raccourci_regexp = str_replace($_match[0], "@{$nom}@", $raccourci_regexp);
			$arguments[] = "{$nom}=" . $_match[0];
		}
		$arguments = '{' . join(',', $arguments) . '}';
	}

	return array($raccourci_regexp, $arguments);
}

/**
 * Détection des items de langue obsolètes d'un module.
 * Cette fonction renvoie un tableau composé des items obsolètes et des items potentiellement obsolètes.
 *
 * @param array  $utilisations Tableau des occurrences d'utilisation d'items de langue dans le code de l'arborescence choisie.
 * @param string $module       Nom du module de langue en cours de vérification.
 * @param array  $items_module Liste des items de langues contenus dans le module de langue en cours de vérification.
 *                             L'index est le raccourci, la valeur la traduction brute.
 *
 * @return array Tableau des items obsolètes ou potentiellement obsolètes. Ce tableau associatif possède une structure
 *               à deux index :
 *               - 'occurrences_non' : liste des items obsolètes;
 *               - 'occurrences_non_mais' : liste des items a priori obsolètes pour le module vérifié mais utilisés avec un autre module;
 *               - 'occurrences_peut_etre' : liste des items potentiellement obsolètes (contexte d'utilisation dynamique).
 */
function reperer_items_non_utilises($utilisations, $module, $items_module) {

	// On boucle sur la liste des items de langue ($items_module) du module en cours de vérification ($module).
	// On teste chaque item pour trouver une utilisation
	$item_non = $item_non_mais = $item_peut_etre = array();
	foreach ($items_module as $_raccourci => $_traduction) {
		// Il faut absolument tester l'item complet soit module:raccourci car sinon
		// on pourrait accepter comme ok un raccourci identique utilisé avec un autre module.
		// Pour cela la valeur de chaque index des sous-tableaux $utilisations est l'item complet
		// (module:raccourci).
		$item = "{$module}:{$_raccourci}";
		if (!in_array($item, $utilisations['items'])) {
			// L'item est soit
			// 1- non utilisé avec le module en cours de vérification
			// 2- non utilisé avec le module en cours de vérification mais utilisé avec un autre module
			// 3- utilise dans un contexte variable

			// On cherche si l'item est détectable dans un contexte variable: on essaye d'avoir le début et la fin
			// qui matche (plus pertinent) et sinon on se contente du début.
			$index_variable = '';
			foreach ($utilisations['raccourcis'] as $_cle => $_valeur) {
				if ($utilisations['variables'][$_cle]) {
					if (
						$utilisations['debuts'][$_cle]
						and (substr($_raccourci, 0, strlen($utilisations['debuts'][$_cle])) === $utilisations['debuts'][$_cle])
					) {
						if ($utilisations['fins'][$_cle]) {
							if (substr($_raccourci, -strlen($utilisations['fins'][$_cle])) === $utilisations['fins'][$_cle]) {
								// On ne peut pas faire mieux, on s'arrête.
								$index_variable = $_cle;
								break;
							}
						} else {
							// On ne s'arrête pas car si on trouve une fin aussi qui matche on obtient une information plus précise
							$index_variable = $_cle;
						}
					}
				}
			}

			if (!$index_variable) {
				if ($items_suspects = array_keys($utilisations['raccourcis'], $_raccourci)) {
					// Cas 2- : l'item est utilise avec un module différent que celui en cours
					// de vérification ce qui peut révéler une erreur.
					// On renvoie les occurrences en cause pour affichage complet en reconstruisant le
					// tableau afin qu'il soit de même format que celui des items peut_etre.
					$occurrences_suspectes = array_intersect_key($utilisations['occurrences'], array_flip($items_suspects));
					foreach ($occurrences_suspectes as $_occurrences) {
						foreach ($_occurrences as $_fichier => $_ligne) {
							foreach ($_ligne as $_no_ligne => $_occurrence) {
								if (!isset($item_non_mais[$_raccourci][$_fichier])) {
									// Première occurrence dans ce fichier
									$item_non_mais[$_raccourci][$_fichier] = $_ligne;
								} elseif (!isset($item_non_mais[$_raccourci][$_fichier][$_no_ligne])) {
									// Cette ligne n'a pas encore d'occurrence
									$item_non_mais[$_raccourci][$_fichier][$_no_ligne] = $_occurrence;
								} else {
									// Cette ligne avait déjà une occurrence
									$item_non_mais[$_raccourci][$_fichier][$_no_ligne] = array_merge($item_non_mais[$_raccourci][$_fichier][$_no_ligne], $_occurrence);
								}
							}
						}
					}
				} else {
					// Cas 1- : on renvoie uniquement la traduction afin de l'afficher dans les résultats.
					$item_non[$_raccourci][] = $_traduction;
				}
			} else {
				// Cas 3- : l'item est utilise dans un contexte variable, on renvoie l'occurrence complète
				$item_peut_etre[$_raccourci] = $utilisations['occurrences'][$index_variable];
			}
		}
	}

	return array(
		'occurrences_non'       => $item_non,
		'occurrences_non_mais'  => $item_non_mais,
		'occurrences_peut_etre' => $item_peut_etre,
	);
}

/**
 * Détection des items de langue utilises mais apparamment non definis.
 * Cette fonction renvoie un tableau composé des items manquants et des items potentiellement manquants.
 *
 * @param array  $utilisations    Tableau des occurrences d'utilisation d'items de langue dans le code de l'arborescence choisie.
 * @param string $module          Nom du module de langue en cours de vérification.
 * @param array  $items_module    Liste des items de langues contenus dans le module de langue en cours de vérification. L'index est
 *                                le raccourci, la valeur la traduction brute.
 * @param array  $fichiers_langue Liste des fichiers de langue 'fr' présent sur site et dans lesquels il est possible de trouver
 *                                certains items de langue.
 *
 * @return array
 */
function reperer_items_non_definis($utilisations, $module, $items_module = array(), $fichiers_langue = array()) {

	// Constitution du tableau de tous les items de langue fr disponibles sur le site et stockage de la liste
	// des modules scannés
	include_spip('inc/texte_mini');
	$tous_lang = $modules_tous_lang = array();
	foreach ($fichiers_langue as $_fichier) {
		$module_tous_lang = preg_match(',/lang/([^/]+)_fr\.php$,i', $_fichier, $m) ? $m[1] : '';
		foreach ($contenu = file($_fichier) as $_texte) {
			if (preg_match_all("#^[\s\t]*['\"]([a-z0-9_]+)['\"][\s\t]*=>(.*)$#i", $_texte, $items, PREG_SET_ORDER)) {
				foreach ($items as $_item) {
					// $_item[1] représente le raccourci
					$tous_lang[$_item[1]][] = array(0 => $_fichier, 1 => $module_tous_lang);
				}
			}
		}
		$modules_tous_lang[] = $module_tous_lang;
	}

	$item_non = $item_non_mais = $item_peut_etre = $item_oui_mais = $complement = array();
	foreach ($utilisations['raccourcis'] as $_cle => $_raccourci) {
		$module_utilise = $utilisations['modules'][$_cle];
		// Il faut absolument tester l'item complet soit module:raccourci car sinon
		// on pourrait vérifier un raccourci identique d'un autre module.
		if (!isset($items_module[$_raccourci]) or ($module_utilise != $module)) {
			$complement[$_raccourci] = array();
			if (!$utilisations['variables'][$_cle]) {
				// L'item est explicite, il n'est ni totalement variable ni suffixé par une partie variable
				if ($module_utilise === $module) {
					// Cas 1: item forcément indefini alors que le module est bien celui en cours de vérification
					// => c'est une erreur !
					$item_non[$_raccourci] = $utilisations['occurrences'][$_cle];
				} else {
					// On vérifie si le raccourci appartient au module en cours de vérification :
					// -- cela peut permettre de trouver une erreur sur le module.
					$raccourci_dans_module = false;
					if (isset($items_module[$_raccourci])) {
						$raccourci_dans_module = true;
					}

					// On vérifie si le raccourci appartient au module utilisé par l'occurrence en cours.
					$module_utilise_verifiable = false;
					$raccourci_dans_module_utilise = false;
					if (
						($module_utilise !== '')
						and in_array($module_utilise, $modules_tous_lang)
					) {
						$module_utilise_verifiable = true;
						if (array_key_exists($_raccourci, $tous_lang)) {
							foreach ($tous_lang[$_raccourci] as $_item_tous_lang) {
								// $_item_tous_lang[1] contient toujours le nom du module exact à savoir
								// pour le core spip, public ou ecrire
								if (!$_item_tous_lang[1]) {
									continue;
								}
								$raccourci_dans_module_utilise =
									$module_utilise ?
									($_item_tous_lang[1] == $module_utilise) :
									(($_item_tous_lang[1] == 'spip') or ($_item_tous_lang[1] == 'ecrire') or ($_item_tous_lang[1] == 'public'));
								if ($raccourci_dans_module_utilise) {
									break;
								}
							}
						}
					}

					$options = array('module' => $module, 'module_utilise' => $module_utilise);
					if ($raccourci_dans_module) {
						// Cas 2 : le raccourci est dans le module en cours de vérification.
						// On donne la priorité au module en cours de vérification. Si le raccourci fait
						// partie de ce module on considère qu'il est plus probable que l'utilisation qui en
						// est faite soit erronée.
						// Néanmoins, si le raccourci est aussi présent dans le module utilisé par l'occurrence
						// en cours de vérification on le précise car cela diminue la probabilité d'une erreur.
						$item_non_mais[$_raccourci] = $utilisations['occurrences'][$_cle];
						$complement[$_raccourci][0] = _T('langonet:complement_definis_non_mais_cas2', $options);
						$complement[$_raccourci][1] =
							$raccourci_dans_module_utilise ?
							_T('langonet:complement_definis_non_mais_cas2_1', $options) :
							($module_utilise_verifiable ?
								_T('langonet:complement_definis_non_mais_cas2_2', $options) :
								_T('langonet:complement_definis_non_mais_cas2_3', $options));
					} else {
						if ($raccourci_dans_module_utilise) {
							// Cas 3 : le raccourci est bien dans le module utilisé mais pas dans le module en cours
							// de vérification. Il y a de grande chance que ce soit ok mais on le notifie
							$item_oui_mais[$_raccourci] = $utilisations['occurrences'][$_cle];
						} else {
							$item_ok = false;
							if ($module_utilise === '') {
								// Cas 3.5
								// On vérifie si le raccourci est dans un module de spip par défaut (spip, public, ecrire, local)
								// mais pas dans le module en cours de vérification, il y a de grande chance que ce soit ok mais on le notifie
								$modules_vides = array('local','public','spip','ecrire'); // Les modules par défaut de SPIP
								if (array_key_exists($_raccourci, $tous_lang)) {
									foreach ($tous_lang[$_raccourci] as $_item_tous_lang) {
										// $_item_tous_lang[1] contient toujours le nom du module exact à savoir
										// pour le core spip, public ou ecrire
										if (!$_item_tous_lang[1]) {
											continue;
										}
										if (in_array($_item_tous_lang[1], $modules_vides)) {
											$item_oui_mais[$_raccourci] = $utilisations['occurrences'][$_cle];
											$item_ok = true;
										}
									}
								}
							}
							if (!$item_ok) {
								// Cas 4 : le raccourci n'est ni dans le module en cours de vérification, ni dans le
								// module de l'occurrence de vérification. Il est donc non défini mais on ne sait pas
								// si cela concerne le module en cours ou pas.
								$item_non_mais[$_raccourci] = $utilisations['occurrences'][$_cle];
								$complement[$_raccourci][0] = _T('langonet:complement_definis_non_mais_cas4', $options);
								$complement[$_raccourci][1] = $module_utilise_verifiable ? '' : _T('langonet:complement_definis_non_mais_cas4_1', $options);
							}
						}
					}
				}
			} else {
				if ($utilisations['indetermines'][$_cle]) {
					// Cas 5 : le raccourci est totalement variable, il n'est pas possible de trouver un
					// raccourci rapprochant dans le module en cours de vérification
					$raccourci_variable = ltrim($_raccourci, '\'".\\');
					$item_peut_etre[$raccourci_variable] = $utilisations['occurrences'][$_cle];
					$complement[$raccourci_variable][0] = _T('langonet:complement_definis_peut_etre_cas5');
				} else {
					// Cas 6 : le raccourci est partiellement variable
					// -- on cherche un item du module en cours de vérification qui pourrait en approcher :
					//    - commencer par un préfixe (debut)
					//    - et éventuellement finir par un suffixe (fin).
					$items_approchants = '';
					$approchants_debut = array();
					// -- on liste les approchants début
					if ($utilisations['debuts'][$_cle]) {
						foreach ($items_module as $_item => $_traduction) {
							if (substr($_item, 0, strlen($utilisations['debuts'][$_cle])) === $utilisations['debuts'][$_cle]) {
								$approchants_debut[] = $_item;
							}
						}
					}
					// -- si on a des approchants debut, on cherche parmi eux des approchants début+fin
					if ($approchants_debut) {
						if ($utilisations['fins'][$_cle]) {
							foreach ($approchants_debut as $_item) {
								if (substr($_item, -strlen($utilisations['fins'][$_cle])) === $utilisations['fins'][$_cle]) {
									$items_approchants = $items_approchants ? ',' . $_item : $_item;
								}
							}
						}
						// Si on a pas trouvé d'items approchant debut+fin on prend uniquement les items approchants début
						if (!$items_approchants) {
							$items_approchants = implode(', ', $approchants_debut);
						}
						$items_approchants = couper($items_approchants, 80);
					}
					$item_peut_etre[$_raccourci] = $utilisations['occurrences'][$_cle];
					$complement[$_raccourci][0] = _T('langonet:complement_definis_peut_etre_cas6');
					$complement[$_raccourci][1] =
						($items_approchants === '') ?
						_T('langonet:complement_definis_peut_etre_cas6_1', array('module' => $module)) :
						_T('langonet:complement_definis_peut_etre_cas6_2', array('module' => $module, 'item' => $items_approchants));
				}
			}
		}
	}

	return array(
		'occurrences_non'       => $item_non,
		'occurrences_non_mais'  => $item_non_mais,
		'occurrences_oui_mais'  => $item_oui_mais,
		'occurrences_peut_etre' => $item_peut_etre,
		'complements'           => $complement,
	);
}
